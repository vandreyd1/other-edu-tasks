package hashmap_test;

public class CustomMapTest {
    public static void main(String[] args) {
        CustomMap<String, Integer> customMap = new CustomMap();
        customMap.put("test", 13);

        customMap.put("test", 15);
        customMap.put("test1", 14);

        customMap.put("hola", 111);

        Integer test = customMap.get("hola");
        System.out.println(test);
    }
}
