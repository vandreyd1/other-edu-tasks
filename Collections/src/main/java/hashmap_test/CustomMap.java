package hashmap_test;

public class CustomMap<K, V> {

  private Entry<K, V>[] table = new Entry[16];

  public void put(K key, V value) {
    int newHash = newHashGenerator(key);
    int index = indexFor(newHash); // table.length с учётом размера массива
    Entry<K, V> entry = table[index];
    if (entry == null) {
      Entry<K, V> newEntry = new Entry<K, V>(key, value, key.hashCode(), null);
      table[index] = newEntry;
    } else {
      boolean isValueChanged = false;
      do {
        if ((entry.key == key || entry.key.equals(key)) && entry.hash == key.hashCode()) {
          entry.value = value;
          isValueChanged = true;
          break;
        }
      } while ((entry = entry.nextEntry) != null);
      if (!isValueChanged) {
        Entry newEntry = new Entry(key, value, key.hashCode(), table[index]);
        table[index] = newEntry;
      }
    }
  }

  public V get(K key) {
//        Entry<K, V> entry;
    return getEntry(newHashGenerator(key), key).getValue();
  }

  Entry<K, V> getEntry(int hash, Object key) {
    Entry<K, V> entry;
    int index = indexFor(hash);
    int length = table.length;
    entry = table[index];
    if (table[index] == null && length > 0) {
      return null;
    } else if (entry.hash == hash && entry.key == key || (key != null && key.equals(entry.key))) {
      return entry;
    } else if ((entry = entry.nextEntry) != null) {
      do {
        if (entry.hash == hash && entry.key == key || (key != null && key.equals(entry.key))) {
          return entry;
        }
      } while (entry.nextEntry != null);
    }
    return null;
  }

  private int indexFor(int newHash) {
    return newHash & (table.length - 1);//newHash ^ (table.length - 1);
  }

  private int newHashGenerator(K key) {
    int hash = 1;
    hash = hash ^ hash >>> 20 ^ hash >>> 12;
    return hash ^ hash >>> 7 ^ hash >>> 3;
  }

  private class Entry<K, V> {

    private K key;
    private V value;
    private int hash;
    private Entry<K, V> nextEntry;

    public Entry(K key, V value, int hash, Entry<K, V> nextEntry) {
      this.key = key;
      this.value = value;
      this.hash = hash;
      this.nextEntry = nextEntry;
    }

    public K getKey() {
      return key;
    }

    public void setKey(K key) {
      this.key = key;
    }

    public V getValue() {
      return value;
    }

    public void setValue(V value) {
      this.value = value;
    }

    public int getHash() {
      return hash;
    }

    public void setHash(int hash) {
      this.hash = hash;
    }

    public Entry<K, V> getNextEntry() {
      return nextEntry;
    }

    public void setNextEntry(Entry<K, V> nextEntry) {
      this.nextEntry = nextEntry;
    }

    @Override
    public String toString() {
      return "Entry{" +
          "key=" + key +
          ", value=" + value +
          ", hash=" + hash +
          ", nextEntry=" + nextEntry +
          '}';
    }
  }
}
