package hashmap_test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapTest {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("1", 1);
        map.put("test", 13);

        System.out.println(map.get("1"));
        System.out.println(map.get("test"));
        System.out.println(map.get("unknown key"));

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        Iterator<Map.Entry<String, Integer>> iterator = entries.iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Integer> next = iterator.next();
            System.out.println("k = "+next.getKey());
            System.out.println("v = "+next.getValue());
        }
        for (Map.Entry entry: map.entrySet()){
            System.out.println("k1 = "+entry.getKey());
            System.out.println("v1 = "+entry.getValue());
        }
    }
}
