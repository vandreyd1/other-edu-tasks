package custom_collection;

import java.util.Collections;
import java.util.Iterator;

public class CustomListTest {
    public static void main(String[] args) {
        CustomList customList = new CustomList();
        for (int i = 0; i < 10; i++) {
            customList.add(i);
        }
        System.out.println("(after adding) currentIndex = " +customList.currentIndex);
        System.out.println(customList);
//        System.out.println(customList.get(9));

        customList.remove(3);
        System.out.println("(after removing) currentIndex = " +customList.currentIndex);
        customList.trimToSize();
        System.out.println("(trimToSize)currentIndex = " +customList.currentIndex);
        System.out.println(customList);

        System.out.println("Iterator:");

        Iterator iterator = customList.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next()+ " ");
        }

        System.out.println();
        System.out.println();

        System.out.println("sort by hash");
        customList.sort(new ComparatorByHash());
        System.out.println(customList);
    }
}
