package custom_collection;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

public class CustomList<T> implements Iterable<T> {
    private Object values[];
    private static final Object emptyMas[] = {};
    public int currentIndex = 0;
//    private Iterator iterator;


    public void add(T i) {
        ensureArraySize();
        values[currentIndex++] = i;
    }


    public CustomList() {
        this.values = emptyMas;
    }

    public Object[] toArray(){
        return values;
    }

    public int size()
    {
        return values.length;
    }

    public void trimToSize(){
        if (currentIndex<values.length){
            if (values.length == 0){
                values = emptyMas;
            } else {
                values = Arrays.copyOf(values, currentIndex);
            }
        }
    }

    private void ensureArraySize() {
        if (currentIndex + 1 > values.length) {
            // расширить память по формуле 3х/2 + 1
            values = Arrays.copyOf(values, 3 * values.length / 2 + 1);
        }
    }

    public Object get(int index) {
        if (index > currentIndex - 1 || index < 0) throw new RuntimeException("index not vaild");
        return values[index];
    }

    public void remove(int index) {
        System.arraycopy(values, index + 1, values, index, currentIndex - 1 - index);
//        values[currentIndex-1] = 0;
//        --currentIndex;
        values[--currentIndex] = 0;
        System.out.println(Arrays.toString(values));
    }

    public void sort(Comparator<? super  T> comparator){
        Arrays.sort((T[]) values, 0 , values.length, comparator);
    }

    @Override
    public String toString() {
        return "CustomList" +
                "values=" + Arrays.toString(values);
    }


    @Override
    public Iterator<T> iterator() {
        currentIndex = 0;
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return currentIndex < values.length;
            }

            @Override
            public T next() {
                return (T)values[currentIndex++];
            }
        };
    }

}
