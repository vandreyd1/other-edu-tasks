package custom_collection;

import java.util.Iterator;
import java.util.List;

public class MyIterator implements Iterable, Iterator {
//    private int[] mas = {2, 4, 3, 21, 423, 345,};
//    CustomList mas = new CustomList();
    private CustomList mas;

    public MyIterator(CustomList mas) {
        this.mas = mas;
    }

    private int curPosition = 0;

    @Override
    public Iterator iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return curPosition < mas.size();
    }

    @Override
    public Object next() {
        return mas.toArray()[curPosition++];
    }
}
