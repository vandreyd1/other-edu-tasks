package custom_collection;

import java.util.Comparator;

public class ComparatorByHash implements Comparator<Object> {
    //todo мб понадобится сделать ещё отдельную логику для instanceof Number и String
    @Override
    public int compare(Object o1, Object o2) {
        return o2.hashCode() - o1.hashCode();
    }
}
