package comparator_test.comparators;

import comparator_test.model.User;

import java.util.Comparator;

public class UserComparatorByAge implements Comparator<User>{
    public int compare(User o1, User o2){
        return o2.getAge() - o1.getAge();
    }
}
