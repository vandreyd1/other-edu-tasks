package comparator_test.tests;

import comparator_test.comparators.UserComparatorByAge;
import comparator_test.comparators.UserComparatorByName;
import comparator_test.model.User;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserComparatorByNameTest {
    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User(12, "Boris"),
                new User(14, "Nikolay"),
                new User(10, "Andrey")
        );
        Collections.sort(users, new UserComparatorByName());
        System.out.println(users);

        Collections.sort(users, new UserComparatorByAge());
        System.out.println(users);

        System.out.println("Comparable: ");
        Collections.sort(users);
        System.out.println(users);
    }
}
