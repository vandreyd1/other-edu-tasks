package copy_test;

import java.util.Arrays;

public class ArrayCopyTest {
    public static void main(String[] args) {
        int mas[] = {3, 5};
        int masNew[] = Arrays.copyOf(mas, 5);
        int[] masNew2 = copyArray(mas);
        System.out.println(Arrays.toString(masNew2));
        int index = mas.length;
        for (int i = 7; i < 10; i++) {
            masNew[index++] = i;
        }

        System.out.println(Arrays.toString(masNew));
    }

    private static int[] copyArray(int[] mas) {
        int masNew2[] = new int[5];
        System.arraycopy(mas, 0, masNew2,0,mas.length);
        return masNew2;
    }
}
