package array_list_test;

public class BlackBox<T>{ //T - от type
    private T obj;

    public BlackBox(T obj) {
        this.obj = obj;
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    @Override
    public String toString() {
        return "BlackBox{" +
                "obj=" + obj +
                '}';
    }
}
