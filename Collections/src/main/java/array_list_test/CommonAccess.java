package array_list_test;

import java.util.ArrayList;
import java.util.List;

public class CommonAccess   {
    public static void main(String[] args) {
//        BlackBox blackBox = new BlackBox(new Integer(12)); //new BlackBox("test string");
//        String obj = (String) blackBox.getObj();
//        System.out.println(obj);
        BlackBox<String> blackBox = new BlackBox("xexeexexex");
        String obj = (String) blackBox.getObj();
        System.out.println(obj);


        List<Long> integerList = new ArrayList();
        for (long i = 0; i < 10; i++) {
            integerList.add(i);
        }
        System.out.println(integerList);
//        addString(integerList);

        System.out.println(getSum(integerList));

        printObjects(12, "Hello there!", 42);
        printObjects(1222, "Hello aa there!", 423;
    }

//    static void addString(List<Integer> integerList){
//        for (int i = 0; i < 10; i++) {
//            integerList.add(String.valueOf(i));
//        }
//    }

    static double getSum(List<? extends Number> numbers){
        double sum = 0;
        for (Number number:numbers){
            sum+=number.doubleValue();
        }
        return sum;

    }
    static <T, T1, T2> void printObjects(T obj, T1 ojb1, T2 obj3){
        System.out.println(obj);
        System.out.println(ojb1);
        System.out.println(obj3);
    }

}
