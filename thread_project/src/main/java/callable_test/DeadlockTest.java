package callable_test;

public class DeadlockTest {
    public static void main(String[] args) {
        final Object obj1 = new Object();
        final Object obj2 = new Object();
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                synchronized (obj1) {
                    try {
                        System.out.println("Object 1 was synchronized by Thread1");
                        Thread.sleep(250);
                        synchronized (obj2) {
                            System.out.println("Object 2 was synchronized by Thread1");
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        };
        Thread thread2 = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (obj2) {
                        System.out.println("Object 2 was synchronized by Thread2");
                        synchronized (obj1) {
                            System.out.println("Object 1 was synchronized by Thread2");
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        };
        thread1.start();
        thread2.start();
    }
}
