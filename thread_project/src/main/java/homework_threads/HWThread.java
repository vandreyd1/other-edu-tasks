package homework_threads;

import com.sun.xml.internal.ws.util.Pool;

/*
Напишите программу, которая каждые 5 секунд отображает на экране данные о времени, прошедшем от начала сессии(запуска программы).
Рассмотрите систему в режиме многопоточности: то-есть 1-ин поток должен считать время(общее) прошедшее с момента запуска программы,
другой поток ориентируясь на данные 1-го потока(в случае если это время кратное 5 сек) - выводит месседж: “сессия бежит N секунд”
Предполагается использование методов wait(), notifyAll().
 */
public class HWThread {

    public static void main(String[] args) throws InterruptedException {
        TimeMonitor timeMonitor = new TimeMonitor();
        FirstThread firstThread = new FirstThread(timeMonitor);
        SecondThread secondThread = new SecondThread(timeMonitor);
//        secondThread.setDaemon(true);
        firstThread.start();
        secondThread.start();
        Thread.sleep(10);

    }
}
