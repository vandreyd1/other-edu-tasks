package homework_threads;

public class SecondThread extends Thread {
    private TimeMonitor timeMonitor;

    public SecondThread(TimeMonitor timeMonitor) {
        this.timeMonitor = timeMonitor;
    }

    @Override
    public void run() {

        try {
            for (int i = 0; i < 115000; i++) {
                Thread.sleep(100);
                timeMonitor.printTime();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
