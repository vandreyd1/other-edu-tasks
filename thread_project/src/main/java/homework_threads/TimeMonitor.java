package homework_threads;

public class TimeMonitor {
    private final long zeroTime = System.currentTimeMillis();
    private long start = zeroTime;
    private long time;

    //    private boolean isTimeChanged = false;
    public synchronized void printTime() {
        try {
            while (time != 0) {
                System.out.printf("сессия бежит %d секунд\n", (Math.round(time / 1000)));
                wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void setTime() {
        try {
            long threadTime = System.currentTimeMillis();
            while ( (threadTime - start) >= 5000 && (threadTime - start)<6000 ) {
                this.time = threadTime - zeroTime;
                start = threadTime;
                threadTime = System.currentTimeMillis();
                notifyAll();
                Thread.sleep(1500);
            }
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        isTimeChanged = true;

    }
}
