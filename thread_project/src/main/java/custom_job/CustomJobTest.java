package custom_job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomJobTest {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(new Customer(1, "n1", "s1"),
                new Customer(2, "n2", "s2"),
                new Customer(3, "n3", "s3"));
        List<CustomJob> customJobs = new ArrayList<CustomJob>();
        for(Customer customer: customers){
            for (int jobID = 0; jobID < 6; jobID++) {
                customJobs.add(new CustomJob(customer,jobID));
            }
        }
        CustomExecutorService customExecutorService = new CustomExecutorService(3);
        for(CustomJob customJob: customJobs){
            customExecutorService.submit(customJob);
        }
    }
}
