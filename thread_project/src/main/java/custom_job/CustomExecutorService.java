package custom_job;

import java.util.concurrent.*;

public class CustomExecutorService extends ThreadPoolExecutor {
    private BlockingDeque<CustomFutureTask> waitJobQueue = new LinkedBlockingDeque<CustomFutureTask>();
    private BlockingDeque<CustomFutureTask> runningJobQueue = new LinkedBlockingDeque<CustomFutureTask>();

    public CustomExecutorService(int nThreads) {
        super(nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        if (task == null) throw new NullPointerException();
        RunnableFuture<T> ftask = newTaskFor(task);
        CustomFutureTask customFutureTask = new CustomFutureTask(task, ftask);
        if (runningJobQueue.contains(customFutureTask)){
            waitJobQueue.add(customFutureTask);
        }else {
            runningJobQueue.add(customFutureTask);
            execute(ftask);
        }

        return ftask;
    }

    @Override
    protected synchronized void afterExecute(Runnable r, Throwable t) {
        for(CustomFutureTask customFutureTask : runningJobQueue){
            if (customFutureTask.getRunnableFuture().equals(r)){
                runningJobQueue.remove(customFutureTask);
                for(CustomFutureTask cf : waitJobQueue){
                    if(cf.getCustomJob().equals(customFutureTask.getCustomJob())){
                        waitJobQueue.remove(cf);
                        submit(cf.getCustomJob());
                        break;
                    }
                }
                break;
            }
        }
    }
}

class CustomFutureTask{
    private Callable customJob;
    private RunnableFuture runnableFuture;

    public CustomFutureTask(Callable customJob, RunnableFuture runnableFuture) {
        this.customJob = customJob;
        this.runnableFuture = runnableFuture;
    }

    public Callable getCustomJob() {
        return customJob;
    }

    public void setCustomJob(Callable customJob) {
        this.customJob = customJob;
    }

    public RunnableFuture getRunnableFuture() {
        return runnableFuture;
    }

    public void setRunnableFuture(RunnableFuture runnableFuture) {
        this.runnableFuture = runnableFuture;
    }

    @Override
    public String toString() {
        return "CustomFutureTask{" +
                "customJob=" + customJob +
                ", runnableFuture=" + runnableFuture +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomFutureTask)) return false;

        CustomFutureTask that = (CustomFutureTask) o;

        return customJob != null ? customJob.equals(that.customJob) : that.customJob == null;
    }

    @Override
    public int hashCode() {
        return customJob != null ? customJob.hashCode() : 0;
    }
}