package custom_job;

import java.util.concurrent.Callable;

public class CustomJob implements Callable<String> {
    private Customer customer;
    private int jobId;

    public CustomJob(Customer customer, int jobId) {
        this.customer = customer;
        this.jobId = jobId;
    }

    public String call() throws Exception {
        String jobResult = String.format("customer %s; job id %s", customer, jobId);
        System.out.println(jobResult);
        Thread.sleep(100);
        return jobResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomJob)) return false;

        CustomJob customJob = (CustomJob) o;

        return customer.equals(customJob.customer);
    }

    @Override
    public int hashCode() {
        return customer.hashCode();
    }
}
