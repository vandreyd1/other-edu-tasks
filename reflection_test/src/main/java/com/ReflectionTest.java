package com;

public class ReflectionTest extends  ParentTest {
    private int in = 20;
    public String str;
    final double d = 3;

    ReflectionTest(String str){
        this.str = str;
    }
    @Override
    public void doSomething(int i){
        super.doSomething(i);
    }

    public void doSomething(int i, String s){
        System.out.printf("i = %d; s = %s",i ,s);
    }
}
