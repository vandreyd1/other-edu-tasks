package com;

import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ReflectionDemo {
    public static void main(String[] args) throws Exception {
        ReflectionTest object = new ReflectionTest("Hello");
        Class aClass = object.getClass();
        System.out.println(aClass.getPackage());

        String className = aClass.getSimpleName();
        System.out.println(className);

        int modifiers = aClass.getModifiers();
        if (Modifier.isPublic(modifiers)){
            System.out.println("public");
        }else if (Modifier.isPrivate(modifiers)){
            System.out.println("private");
        }
        Field in = aClass.getDeclaredField("in");
        in.setAccessible(true);
        Integer o = (Integer) in.get(object);
        System.out.println(o);
        in.set(object, 30);
        o = (Integer) in.get(object);
        System.out.println(o);

        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field f : declaredFields){
            if(Modifier.isPrivate(f.getModifiers())){
                f.setAccessible(true);
            }

            if(f.getType() == String.class){
                f.set(object, "Hi");
            }
            System.out.println(f.getName()+ " = " + f.get(object));
        }

        Method doSomething = aClass.getDeclaredMethod("doSomething", int.class);
        doSomething.invoke(object, new Object[] {56});

        Method doSomething1 = aClass.getDeclaredMethod("doSomething", int.class, String.class);
        doSomething1.invoke(object, new Object[] {43, "Aloha"});

        Method[] declaredMethods = aClass.getDeclaredMethods();
        for (Method m: declaredMethods){
            Class<?>[] parameterTypes = m.getParameterTypes();
            for( Class type: parameterTypes){
                System.out.println(type.getSimpleName());
            }
        }


    }
}


