package niotest;

import java.nio.file.*;

public class WatcherTest {

  public static void main(String[] args) throws java.io.IOException, InterruptedException {
   Path path = java.nio.file.Paths.get("D:\\learn Java\\");
    java.nio.file.WatchService watchService = path.getFileSystem().newWatchService();

    //на что реагируем:
    java.nio.file.WatchKey register = path
        .register(watchService, java.nio.file.StandardWatchEventKinds.ENTRY_CREATE,
            java.nio.file.StandardWatchEventKinds.ENTRY_DELETE);

    while (true) {
      java.nio.file.WatchKey take = watchService.take();
      java.util.List<java.nio.file.WatchEvent<?>> watchEvents = take.pollEvents();
      for (java.nio.file.WatchEvent watchEvent : watchEvents) {
        switch (watchEvent.kind().name()) {
          case "ENTRY_CREATE":
            System.out.println("ENTRY_CREATE works");
            break;
          case "ENTRY_DELETE":
            System.out.println("ENTRY_DELETE works");
            break;
        }
      }
      take.reset();
    }
  }
}
