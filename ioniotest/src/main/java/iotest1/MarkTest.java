package iotest1;

public class MarkTest {

  public static void main(String[] args) throws java.io.IOException {
    java.io.File file = new java.io.File("2.txt");
    java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
    java.io.BufferedInputStream bufferedInputStream = new java.io.BufferedInputStream(
        fileInputStream);

    System.out.println((char)bufferedInputStream.read());
    System.out.println((char)bufferedInputStream.read());
    System.out.println("mark>>>");
    bufferedInputStream.mark(16);
    System.out.println((char)bufferedInputStream.read());
    System.out.println((char)bufferedInputStream.read());
    bufferedInputStream.reset();
    System.out.println((char)bufferedInputStream.read());
    System.out.println((char)bufferedInputStream.read());
  }

}
