package iotest1;

import java.io.*;

public class Test1 {

  public static void main(String[] args) throws java.io.IOException {
    File file = new File("1.txt");
    try (FileOutputStream fileOutputStream = new java.io.FileOutputStream(file, true)) {
      fileOutputStream.write("hello from test 1!\n".getBytes());
      fileOutputStream.flush();
    }

    try(java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file)) {
      java.io.BufferedInputStream bufferedInputStream = new java.io.BufferedInputStream(
          fileInputStream);
      byte[] buffer = new byte[1024];

      while (bufferedInputStream.read(buffer) != -1) {
        System.out.println(new String(buffer));
      }
//    bufferedInputStream.close();
    }
  }
}
