package iotest1;

public class Test2 {

  public static void main(String[] args) throws java.io.IOException {
    java.io.File file = new java.io.File("1.txt");
    try (java.io.FileWriter fileWriter = new java.io.FileWriter(file, true)) {
      fileWriter.write("ZZZ");
      fileWriter.flush();
    }

    try (java.io.FileReader fileReader = new java.io.FileReader(file);
        java.io.BufferedReader bufferedReader = new java.io.BufferedReader(fileReader)) {
      String buffer;
      while ((buffer = bufferedReader.readLine()) != null) {
//        bufferedReader.readLine();
        System.out.println(buffer);
      }
    }
  }
}
