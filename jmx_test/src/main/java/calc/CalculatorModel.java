package calc;

public class CalculatorModel implements CalculatorMBean {

  private int firstNumber;
  private int secondNumber;

  public CalculatorModel(int firstNumber, int secondNumber) {
    this.firstNumber = firstNumber;
    this.secondNumber = secondNumber;
  }

  @Override
  public int getFirstNumber() {
    return firstNumber;
  }

  @Override
  public void setFirstNumber(int firstNumber) {
    this.firstNumber = firstNumber;
  }

  @Override
  public int getSecondNumber() {
    return secondNumber;
  }

  @Override
  public void setSecondNumber(int secondNumber) {
    this.secondNumber = secondNumber;
  }

  @Override
  public int add() {

    return firstNumber + secondNumber;
  }

  @Override
  public String toString() {
    return "CalculatorModel{" +
        "firstNumber=" + firstNumber +
        ", secondNumber=" + secondNumber +
        '}';
  }
}
