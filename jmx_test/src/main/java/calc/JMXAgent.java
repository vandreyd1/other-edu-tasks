package calc;

import javax.management.*;
import java.lang.management.ManagementFactory;

public class JMXAgent {
    public static void main(String[] args) throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
        //сервер для регистрации mbeans
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        //создание самой модельки "калькулятора" - c числами заданными хардкодом(1 и 2)
        CalculatorMBean calculatorMBean = new CalculatorModel(1, 2);
        //определение имени для нашего мбина на уровне JMX менеджера(catalog, folder, format - standard; %catalog%:name=%folder%)
        ObjectName objectName = new ObjectName("jmx-test:name=first-mbean");
        //создание стандартного мбина
        StandardMBean standardMBean = new StandardMBean(calculatorMBean, CalculatorMBean.class);
        //регистрация мбина
        platformMBeanServer.registerMBean(standardMBean, objectName);
        //"зависание" с помощью бесконечного цикла
        for (; ; ) {

        }
    }
}